**The blooming florist** is an application that efficiently manages, in a greenhouse, the growth of flowers. It will also offer the chance to manage employees and their activities.
Hope you enjoy!

Usage instructions:
	1. Download the jar file
	2. Run it.

This application is developed by Elena Alessandrini, Jean Paul Elleri and Igor Lirussi